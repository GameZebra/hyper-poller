DROP SCHEMA IF EXISTS `hyper-poller` ;

CREATE SCHEMA IF NOT EXISTS `hyper-poller` DEFAULT CHARACTER SET utf8 ;
USE `hyper-poller` ;
DROP TABLE IF EXISTS `hyper-poller`.`Company` ;

-- -----------------------------------------------------
-- Table `hyper-poller`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hyper-poller`.`Customer` ;

CREATE TABLE IF NOT EXISTS `hyper-poller`.`Customer` (
  `customerId` INT NOT NULL AUTO_INCREMENT,
  `customerUuid` INT NOT NULL UNIQUE,
  `name` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  PRIMARY KEY (`customerId`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `hyper-poller`.`Company` (
  `companyId` INT NOT NULL AUTO_INCREMENT,
  `companyUuid` VARCHAR(12) NULL UNIQUE,
  `companyName` VARCHAR(45) NULL UNIQUE,
  `companyAddress` VARCHAR(45) NULL,
  PRIMARY KEY (`companyId`))
ENGINE = InnoDB;


DROP TABLE IF EXISTS `hyper-poller`.`Store` ;

CREATE TABLE IF NOT EXISTS `hyper-poller`.`Store` (
  `storeId` INT NOT NULL AUTO_INCREMENT,
  `storeName` VARCHAR(45) NULL UNIQUE,
  `storeAddress` VARCHAR(45) NULL,
  `companyId` INT,
  PRIMARY KEY (`storeId`),
    FOREIGN KEY (`companyId`) REFERENCES `Company`(`companyId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;


DROP TABLE IF EXISTS `hyper-poller`.`Carddetails` ;

CREATE TABLE IF NOT EXISTS `hyper-poller`.`Carddetails` (
  `cardId` INT NOT NULL AUTO_INCREMENT,
  `cardNumber` VARCHAR(20) NOT NULL UNIQUE,
  `cardType` VARCHAR(45) NULL,
  `contactless` BINARY NULL,
  PRIMARY KEY (`cardId`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `hyper-poller`.`Invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hyper-poller`.`Invoice` ;

CREATE TABLE IF NOT EXISTS `hyper-poller`.`Invoice` (
  `invoiceId` INT NOT NULL AUTO_INCREMENT,
  `datetime` DATETIME NOT NULL UNIQUE,
  `storeId` INT NULL,
  `total` DECIMAL(20,2) NULL,
  `customerId` INT NULL,
  `payment` VARCHAR(15) NULL,
  `cardId` INT NULL,
  PRIMARY KEY (`invoiceId`),
  -- INDEX `customerId_idx` (`customerId` ASC) ,
  -- INDEX `cardId_idx` (`cardId` ASC) ,
  -- INDEX `storeId_idx` (`storeId` ASC) ,
	FOREIGN KEY (`storeId`) REFERENCES `Store`(`storeId`)
	ON DELETE NO ACTION
	ON UPDATE CASCADE,    
    FOREIGN KEY (`cardId`) REFERENCES `Carddetails`(`cardId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
	FOREIGN KEY (`customerId`) REFERENCES `Customer`(`customerId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB;



-- -----------------------------------------------------
-- Table `hyper-poller`.`Receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `hyper-poller`.`Receipt` ;

CREATE TABLE IF NOT EXISTS `hyper-poller`.`Receipt` (
  `receiptId` INT NOT NULL AUTO_INCREMENT,
  `datetime` DATETIME NOT NULL UNIQUE,
  `total` DECIMAL(20,2) NULL,
  `payment` VARCHAR(15) NULL,
  `cardId` INT NULL,
  `storeId` INT NOT NULL,
  PRIMARY KEY (`receiptId`),
  -- INDEX `storeId_idx` (`storeId` ASC) ,
  -- INDEX `cardId_idx` (`cardId` ASC) ,
  -- CONSTRAINT `cardId`
    FOREIGN KEY (`storeId`) REFERENCES `Store`(`storeId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,    
    FOREIGN KEY (`cardId`) REFERENCES `Carddetails`(`cardId`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
    )
ENGINE = InnoDB;



-- SET SQL_MODE=@OLD_SQL_MODE;
-- SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
-- SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
