Overview
HyperPoller is a module of HyperPayments - a product for processing and 
analyzing data from POS terminals. The HyperPoller module is responsible 
for deserializing data from files and storing it in a database.

Specification
HyperPoller is a console application that deserializes XML files and stores
data in a MySQL database.
