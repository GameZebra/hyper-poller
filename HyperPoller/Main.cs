﻿using System.IO;
using System.Xml.Serialization;
using HyperPoller.Models;
using HyperPoller.Database;

namespace std
{
    static class Program
    {
        static void Main()
        {
            Database obj = new Database();
            var mySerializer = new XmlSerializer(typeof(Company));
            var myFileStream = new FileStream("../../../../../2019-10-01-brainbox.xml", FileMode.Open);

            var myobject = (Company)mySerializer.Deserialize(myFileStream);
            obj.InsertAll(myobject);
        }
    }
}
