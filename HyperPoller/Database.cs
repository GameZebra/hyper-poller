﻿using HyperPoller.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace HyperPoller.Database
{
    public class Database
    {
        const string connectionStr = @"Server=localhost;Database=hyper-poller;User Id=root;Password=;";
        private readonly IDbConnection connection = new MySqlConnection(connectionStr);

        public Database()
        {
            string path = @"..\..\..\..\..\hyper-poller\database\database.sql";
            string createDatabase = File.ReadAllText(path);

            connection.Open();
            Console.WriteLine("Connection open");
            Console.WriteLine();

            IDbCommand dbCommand = connection.CreateCommand();
            dbCommand.CommandText = createDatabase;
            dbCommand.ExecuteNonQuery();
            connection.Close();
            Console.WriteLine("Database exist");
        }

        public void InsertCompany(Company company)
        {
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
            @"
            INSERT INTO company (companyUuid, companyName, companyAddress) 
            VALUES (@companyUuid, @companyName, @companyAddress)
            ";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@companyUuid";
            parameter.Value = company.Uuid;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@companyName";
            parameter.Value = company.Name;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@companyAddress";
            parameter.Value = company.Adress;
            command.Parameters.Add(parameter);

            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
            Console.WriteLine("Company insert successfully");
            
        }

        public void InsertStore(Company company)
        {
            int companyId = 0;
            IDbCommand command = connection.CreateCommand();
            command.CommandText =
                @"SELECT `companyId` FROM `company` 
                    WHERE `companyUuid` = @companyUuid";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@companyUuid";
            parameter.Value = company.Uuid;
            command.Parameters.Add(parameter);

            connection.Open();
            IDataReader reader = command.ExecuteReader();
            reader.Read();
            companyId = ((int)reader["companyId"]);
            connection.Close();

            for (int i = 0; i < company.Stores.StoreList.Count; i++)
            {
                command = connection.CreateCommand();
                command.CommandText =
                    @"
                    INSERT INTO store (storeName, storeAddress, companyId) 
                    VALUES (@storeName, @storeAddress, @companyId)
                    ";

                parameter = command.CreateParameter();
                parameter.ParameterName = "@storeName";
                parameter.Value = company.Stores.StoreList[i].Name;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@storeAddress";
                parameter.Value = company.Stores.StoreList[i].Address;
                command.Parameters.Add(parameter);

                parameter = command.CreateParameter();
                parameter.ParameterName = "@companyId";
                parameter.Value = companyId;
                command.Parameters.Add(parameter);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                Console.WriteLine("Store insert successfully");
            }
            Console.WriteLine("All stores in");
            Console.WriteLine();
        }
        public void InsertReceipt(Company company)
        {
            int storeId = 0;
            for (int i = 0; i < company.Stores.StoreList.Count; i++)
            {
                IDbCommand command = connection.CreateCommand();
                command.CommandText =
                    @"SELECT `storeId` FROM `store` 
                        WHERE `storeName` = @storeName";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@storeName";
                parameter.Value = company.Stores.StoreList[i].Name;
                command.Parameters.Add(parameter);
                connection.Open();
                IDataReader reader = command.ExecuteReader();
                reader.Read();
                storeId = ((int)reader["storeId"]);
                connection.Close();

                for (int j = 0; j < company.Stores.StoreList[i].Receipts.ReceiptList.Count; j++)
                {
                    command = connection.CreateCommand();
                    command.CommandText =
                        @"
                    INSERT INTO receipt (datetime, total, payment, cardId, storeId) 
                    VALUES (@datetime, @total, @payment, @cardId, @storeId)
                    ";

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@datetime";
                    parameter.Value = company.Stores.StoreList[i].Receipts.ReceiptList[j].DateTime;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@total";
                    parameter.Value = company.Stores.StoreList[i].Receipts.ReceiptList[j].Total;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@payment";
                    parameter.Value = company.Stores.StoreList[i].Receipts.ReceiptList[j].Payment;
                    command.Parameters.Add(parameter);

                    if(company.Stores.StoreList[i].Receipts.ReceiptList[j].Payment == "card")
                    {
                        IDbCommand command1 = connection.CreateCommand();
                        command1.CommandText =$"SELECT `cardId` FROM `carddetails` WHERE `cardNumber` = {company.Stores.StoreList[i].Receipts.ReceiptList[j].CardDetails.Number}";

                        connection.Open();
                        reader = command1.ExecuteReader();
                        reader.Read();
                        int cardId = ((int)reader["cardId"]);
                        connection.Close();

                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@cardId";
                        parameter.Value =cardId ;
                        command.Parameters.Add(parameter);
                    }
                    else
                    {
                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@cardId";
                        parameter.Value = null;
                        command.Parameters.Add(parameter);
                    }


                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@storeId";
                    parameter.Value = storeId;
                    command.Parameters.Add(parameter);

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                    Console.WriteLine("Receipt insert successfully");
                }
            }
            Console.WriteLine("All receipts in");
            Console.WriteLine();
        }

        public void InsertCardDetails (Company company)
        {
            for(int i = 0; i< company.Stores.StoreList.Count; i++)
            {
                for(int j =0; j< company.Stores.StoreList[i].Receipts.ReceiptList.Count; j++)
                {
                    if (company.Stores.StoreList[i].Receipts.ReceiptList[j].Payment == "card")
                    {
                        IDbCommand command = connection.CreateCommand();
                        try
                        {
                            command.CommandText = $"INSERT INTO carddetails (cardNumber, cardType, contactless) " +
                                $"VALUES ('{company.Stores.StoreList[i].Receipts.ReceiptList[j].CardDetails.Number}', " +
                                $"'{company.Stores.StoreList[i].Receipts.ReceiptList[j].CardDetails.CardType}', " +
                                $"{company.Stores.StoreList[i].Receipts.ReceiptList[j].CardDetails.IsContactless})";
                            connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                            Console.WriteLine("Insert card successfully");
                        }
                        catch 
                        {
                            Console.WriteLine("Card already in");
                            connection.Close();
                        }
                    }
                }
                for (int j = 0; j < company.Stores.StoreList[i].Invoices.InvoiceList.Count; j++)
                {
                    if (company.Stores.StoreList[i].Invoices.InvoiceList[j].Payment == "card")
                    {
                        IDbCommand command = connection.CreateCommand();
                        try
                        {
                            command.CommandText = $"INSERT INTO carddetails (cardNumber, cardType, contactless) " +
                                $"VALUES ('{company.Stores.StoreList[i].Invoices.InvoiceList[j].CardDetails.Number}', " +
                                $"'{company.Stores.StoreList[i].Invoices.InvoiceList[j].CardDetails.CardType}', " +
                                $"{company.Stores.StoreList[i].Invoices.InvoiceList[j].CardDetails.IsContactless})";
                            connection.Open();
                            command.ExecuteNonQuery();
                            connection.Close();
                        }
                        catch
                        {
                            Console.WriteLine("exist");
                            connection.Close();
                        }
                    }
                }
            }
            Console.WriteLine("Carddetails");
        }

        public void InsertCustomer (Company company)
        {
            for(int i = 0; i < company.Stores.StoreList.Count; i++)
            {
                for(int j = 0; j< company.Stores.StoreList[i].Invoices.InvoiceList.Count; j++)
                {
                    try
                    {
                        IDbCommand command = connection.CreateCommand();
                        command.CommandText = @"INSERT INTO customer (customerUuid, name, address) VALUES(@customerUuid, @name, @address)";
                        IDataParameter parameter = command.CreateParameter();
                        parameter.ParameterName = "@customerUuid";
                        parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].Customer.Uuid;
                        command.Parameters.Add(parameter);

                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@name";
                        parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].Customer.Name;
                        command.Parameters.Add(parameter);

                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@address";
                        parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].Customer.Address;
                        command.Parameters.Add(parameter);

                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                        Console.WriteLine("Customer added");
                    }
                    catch
                    {
                        Console.WriteLine("already in");
                        connection.Close();
                    }
                }
            }
            Console.WriteLine("All customers in");
             
        }

        public void InsertInvoice(Company company)
        {
            int storeId = 0;
            for (int i = 0; i < company.Stores.StoreList.Count; i++)
            {
                IDbCommand command = connection.CreateCommand();
                command.CommandText =
                    @"SELECT `storeId` FROM `store` 
                        WHERE `storeName` = @storeName";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@storeName";
                parameter.Value = company.Stores.StoreList[i].Name;
                command.Parameters.Add(parameter);
                connection.Open();
                IDataReader reader = command.ExecuteReader();
                reader.Read();
                storeId = ((int)reader["storeId"]);
                connection.Close();

                for (int j = 0; j < company.Stores.StoreList[i].Invoices.InvoiceList.Count; j++)
                {
                    command = connection.CreateCommand();
                    command.CommandText =
                        @"
                    INSERT INTO invoice (datetime, total, payment, cardId, storeId, customerId) 
                    VALUES (@datetime, @total, @payment, @cardId, @storeId, @customerId)
                    ";

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@datetime";
                    parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].DateTime;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@total";
                    parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].Total;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@payment";
                    parameter.Value = company.Stores.StoreList[i].Invoices.InvoiceList[j].Payment;
                    command.Parameters.Add(parameter);

                    if (company.Stores.StoreList[i].Invoices.InvoiceList[j].Payment == "card")
                    {
                        IDbCommand command2 = connection.CreateCommand();
                        command2.CommandText = $"SELECT `cardId` FROM `carddetails` WHERE `cardNumber` = {company.Stores.StoreList[i].Invoices.InvoiceList[j].CardDetails.Number}";

                        connection.Open();
                        reader = command2.ExecuteReader();
                        reader.Read();
                        int cardId = ((int)reader["cardId"]);
                        connection.Close();

                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@cardId";
                        parameter.Value = cardId;
                        command.Parameters.Add(parameter);
                    }
                    else
                    {
                        parameter = command.CreateParameter();
                        parameter.ParameterName = "@cardId";
                        parameter.Value = null;
                        command.Parameters.Add(parameter);
                    }

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@storeId";
                    parameter.Value = storeId;
                    command.Parameters.Add(parameter);

                    IDbCommand command1 = connection.CreateCommand();
                    command1.CommandText = $"SELECT `customerId` FROM `customer` WHERE `customerUuid` = {company.Stores.StoreList[i].Invoices.InvoiceList[j].Customer.Uuid}";

                    connection.Open();
                    reader = command1.ExecuteReader();
                    reader.Read();
                    int customerId = ((int)reader["customerId"]);
                    connection.Close();

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@customerId";
                    parameter.Value = customerId;
                    command.Parameters.Add(parameter);

                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                    Console.WriteLine("Invoice insert successfully");
                }
                Console.WriteLine($"Store {i} finished");
            }
            Console.WriteLine("All invoices");
        }
        public void InsertAll(Company company)
        {
            InsertCompany(company);
            InsertStore(company);
            InsertCardDetails(company);
            InsertCustomer(company);
            InsertReceipt(company);
            InsertInvoice(company);
        }
    }
}
