﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("receipts")]
    public class Receipts
    {
        [XmlElement("receipt")]
        public List<Receipt> ReceiptList { get; set; }
    }
}
