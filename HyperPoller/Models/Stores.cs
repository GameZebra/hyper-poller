﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("stores")]
    public class Stores
    {
        [XmlElement("store")]
        public List<Store> StoreList { get; set; }
    }
}
