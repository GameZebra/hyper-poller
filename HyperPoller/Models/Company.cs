﻿using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlRoot("company")]
    public class Company
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Adress { get; set; }

        [XmlAttribute("uuid")]
        public string Uuid { get; set; }

        [XmlElement("stores")]
        public Stores Stores { get; set; }
    }
}
