﻿using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("carddetails")]
    public class CardDetails
    {
        [XmlElement("cardtype")]
        public string CardType { get; set; }

        [XmlElement("number")]
        public string Number { get; set; }

        [XmlElement("contactless")]
        public bool IsContactless { get; set; }
    }
}
