﻿
using System;
using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("invoice")]
    public class Invoice
    {
        [XmlElement("total")]
        public string Total { get; set; }

        [XmlElement("datetime")]
        public DateTime DateTime { get; set; }

        [XmlElement("payment")]
        public string Payment { get; set; }

        [XmlElement("carddetails")]
        public CardDetails CardDetails { get; set; }

        [XmlElement("customer")]
        public Customer Customer { get; set; }
    }
}
