﻿using System;
using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("receipt")]
    public class Receipt
    {
        [XmlElement("total")]
        public string Total { get; set; }

        [XmlElement("datetime")]
        public DateTime DateTime { get; set; }
   
        [XmlElement("payment")]
        public string Payment { get; set; }

        [XmlElement("carddetails")]
        public CardDetails CardDetails { get; set; }
    }
}
