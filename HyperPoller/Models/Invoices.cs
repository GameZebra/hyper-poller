﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("invoices")]
    public class Invoices
    {
        [XmlElement("invoice")]
        public List<Invoice> InvoiceList { get; set; }
    }
}
