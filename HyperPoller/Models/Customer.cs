﻿using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("customer")]
    public class Customer
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("address")]
        public string Address { get; set; }

        [XmlElement("uuid")]
        public string Uuid { get; set; }
    }
}
