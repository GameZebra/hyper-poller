﻿using System.Xml.Serialization;

namespace HyperPoller.Models
{
    [XmlType("store")]
    public class Store
    {
        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("address")]
        public string Address { get; set; }

        [XmlElement("receipts")]
        public Receipts Receipts { get; set; }

        [XmlElement("invoices")]
        public Invoices Invoices { get; set; }
    }
}
